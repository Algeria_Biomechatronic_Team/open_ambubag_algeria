## Respiratory flow chart ##

| Key Value |  Signification   |     Description      | Breath | Formula | Average Value [^note1] |
| ----------| ---------------- | --------------------- | ------ | ------- | ------------- |
|     TV    | Tidal Volume     | Volume of air breathed  during a single breath at rest | Normal |  | 500|
| IRV | Inspiration Reserve Volume | Maximum Volume of air that can be additionally inspired  after a tidal | Forced  | | 3000|
| ERV | Expiration Reserve Volume | Maximum Volume of air that can be additionally expired  after a tidal | Forced | | 1100 |
| RV | Residual Volume | Volume of air remaining in the Lungs after a maximum Expiration  |Lungs reserve |  | 1200 | 

### Calculated values ###
| Key Value |  Signification   |     Description       | Breath | Formula | Average Value |
| ----------| ---------------- | --------------------- | ------ | ------- | ------------- |
| VC | Vital Capacity|Maximum volum of air  during a forced  breath | Forced | VC = TV + IRV+ERV | 4600 |
| TLC | Total Lung Capacity | Total volume of air that lungs can hold | Lungs capacity | TLC= TV + IRV+ERV+RV | 5800 |
| IC | Inspiration Capacity | Maximum volum of air  that lungs can inspire | Lungs capacity | IC = TV +IRV | 3500 |
| FRC | Functional Residual Capacity |Maximum Air volume remaining in the lungs after simple breath  | Lungs capacity | FRC= ERV+ RV | 2300 |
 




## Lung Volumes  [^note2]
![Lung_Volumes ](/uploads/2ba49509da66c490fd1bfcc35fdfa2c0/Lung_Volumes.png)


## Tidal Volume   [^note3]
Tidal Volume estimation, in  function of Height, Sex, Weight and Lung disease( from 4 to 8 ml/kg) :

[![Tidal_volume_prediction](/uploads/72a25a8a95f6765526f2bd68952935b1/Tidal_volume_prediction.png)


### Normal Respiratory Rates (RR or BPM)   [^note4]
 or : Breath Per Minute (BPM)
 
 | Age               |  Breath/Min| 
 | ------------------| -----------|
 | Newborn to 6 weeks           | 30-60 |
 | Infant (6 weeks to 6 months) | 25-40 |
 | Toddler ( 1 to 3  years)     | 20-30 |
 | Young Children ( 3 to 6 years) | 20-25 |
 | Older Children (10 to 14 years) | 15-20 |
 | Adults | 12-20 |

 

 


[^note1]: Average values for normal Mal /70kg given only for explanation
[^note2]: Lung Volumes  [drawittoknowit Medical and Biological Sciences, Volume Capacity signifiance](https://www.drawittoknowit.com/course/physiology/glossary/physiological-process/lung-volumes-capacities)
[^note3]: PBW, TV, BPM, ... [Excel file Lungs volumes estimation](/uploads/c21a33b6ae050d41dcccf4c292df5d75/Lung_volumes_Parameters__Analysis_V0_010420.xlsx) 
[^note4]: Normal Respiratory Rates [Mosby’s Critical Care Nursing Reference, 2002; Perry & Potter, 2006](https://www.google.com/search?q=normal+respiratory+rate+in+mosby%E2%80%99s+critical+care+nursing+reference+2002+perry+%26+potter+2006&rlz=1C1CHBD_frDZ846DZ846&hl=fr&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj03MSzuM7oAhW55OAKHfMVAuMQ_AUoAXoECAsQAw&biw=1745&bih=837)
