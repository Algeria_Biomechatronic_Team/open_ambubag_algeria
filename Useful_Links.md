# Useful Links

## Kackernoon Info about similar projects
[Kackernoon Info about similar projects](https://hackernoon.com/open-source-ventilator-projects-status-challenges-how-you-can-help-j3sw3wy1)


## Ventilation Specifications
[Ventilation Specifications](https://e-vent.mit.edu/clinical/key-ventilation-specifications/)


## Ventilation artificilelle les fondamentaux
[Ventilation artificilelle les fondamentaux](https://sofia.medicalistes.fr/spip/IMG/pdf/Ventilation_artificilelle_les_fondamentaux.pdf)

## List of opensource Ventilator projects
[List of opensource Ventilator projects](https://docs.google.com/spreadsheets/d/1FA_tSr2jVEGb1-I2UJGqF33Tn55rlWWfMto5w-PDPJg/edit#gid=0)

## VentilAid

[VentilAid](https://gitlab.com/Urbicum/ventilaid)
